SELECT id, cat_id, url, html, htmljs, markup FROM urls where html = '';

SELECT id, cat_id, url, length(html) FROM urls ORDER BY length(html) desc

DELETE FROM urls WHERE length(html) > 3714680;

SELECT id, cat_id, url, length(url) FROM urls ORDER BY length(url) desc;

DELETE FROM urls WHERE length(url) > 180;

SELECT u.cat_id as id, c.name, count(u.id) FROM urls u
    INNER JOIN categories c ON u.cat_id= c.id
GROUP BY u.cat_id, c.name
ORDER BY cat_id


SELECT "u"."cat_id", "category"."name",
       COUNT(DISTINCT "u"."cat_id") FROM "urls" "u", "categories" "category"
WHERE "u"."cat_id" = "category"."id"
GROUP BY "u"."cat_id", "category"."name"


SELECT id, url_id FROM url_html WHERE html_js is null or html_js = '';

SELECT u.id, u.cat_id, u.lang FROM url_texts LEFT JOIN urls u on url_texts.url_id = u.id
WHERE raw_text is null or raw_text = '' or title is null or title = '' or lang!='ru';


SELECT u.id, u.cat_id, u.lang FROM url_texts LEFT JOIN urls u on url_texts.url_id = u.id
WHERE title is null or title = '';

SELECT u.id, u.cat_id, u.lang, ut.raw_text FROM url_texts ut INNER JOIN urls u on ut.url_id = u.id
WHERE u.lang != 'ru' order by u.lang;

SELECT u.id, u.cat_id, ut.raw_text FROM url_texts ut INNER JOIN urls u on ut.url_id = u.id
WHERE u.cat_id = 8 AND raw_text ilike '%страхование%';

SELECT u.id, u.cat_id, ut.raw_text FROM url_texts ut INNER JOIN urls u on ut.url_id = u.id
WHERE u.cat_id = 38 AND raw_text ilike '%страхование%';

SELECT id, raw_text FROM url_texts  WHERE raw_text ilike '%браузер устарел%';

UPDATE url_texts
SET raw_text = replace(raw_text, 'Ваш браузер устарел Вы пользуетесь устаревшим браузером, который не поддерживает современные веб-стандарты и представляет угрозу вашей безопасности. Пожалуйста, установите современный браузер: Chrome Firefox Opera IE', '')
WHERE raw_text ilike '%браузер устарел%';

SELECT u.id, u.cat_id, ut.raw_text, length(ut.raw_text) FROM url_texts ut INNER JOIN urls u on ut.url_id = u.id
ORDER BY length(ut.raw_text);


DELETE FROM urls
where id in ( SELECT url_id FROM url_texts WHERE   raw_text ilike '%є%');

DELETE FROM urls
where id in ( SELECT url_id FROM url_texts WHERE   raw_text ilike '%сайт заблокирован%');

DELETE FROM urls
where id in ( SELECT url_id FROM url_texts WHERE   raw_text ilike '%Срок регистрации домена%');

DELETE FROM urls
where id in ( SELECT url_id FROM url_texts WHERE
    raw_text ilike '%в стадии разработки%' or raw_text ilike '%domain is for sale%');

DELETE FROM urls
where id in ( SELECT url_id FROM url_texts WHERE
    raw_text ilike '%Сайт обслуживается в REG.RU%'
    or raw_text ilike '%403 Forbidden%'
    or raw_text ilike '%MySQL Query Error:%'
    or raw_text ilike '%ОШИБКА Данной страницы НЕ СУЩЕСТВУЕТ!%'
    or raw_text ilike '%Если вы являетесь владельцем этого домена%'
    or raw_text ilike '%using Sedo Domain Parking%'
    or raw_text ilike '%Guarantee Lightning fast incoming domain transfer Payments%'
    or length(raw_text) < 350
    );

DELETE FROM urls WHERE lang != 'ru';



SELECT id, url, host, is_main, cat_id FROM urls WHERE host in
(SELECT host FROM urls GROUP BY host HAVING count(host) > 1)
ORDER BY host;

/* FIND non-main urls */
SELECT id, url, host, is_main, cat_id FROM urls
WHERE length(replace(url, 'www.','')) > length(host) + 9
AND is_main=True
ORDER BY host;

DELETE FROM urls where url like '%403.shtml';

SELECT id, url, host, is_main, cat_id from urls where host like '%timeweb%';

SELECT c.url, ut.*
FROM urls c
    LEFT JOIN  url_texts ut on c.id = ut.url_id;

SELECT id, url, cat_id, host, markup, loaded, load_err FROM urls
WHERE id in (945, 3040, 3089, 3113, 3169, 3233, 3279, 3295, 3697, 3814, 3869, 5093, 5107, 5109, 5125, 5126, 5143, 5240, 5251, 5902, 5989, 6243, 6298, 6405, 6749, 6776, 6934, 7139, 7173, 7277, 8250, 8561, 9040, 9472, 9540, 9582, 10041, 10143, 10295, 10412, 10478, 10567, 10906, 11163, 11414, 11426, 11555, 11615, 12099, 12615, 12617, 12720, 12867, 13092, 13116, 13380, 13444, 13478, 13754, 14235, 14266, 14269, 14335, 14386, 14879, 15110, 15290, 15406, 16011, 16060, 16143, 16318, 16342, 16575, 16777, 16794, 16906, 16908, 17052, 17399, 17527, 17573, 17784, 17868, 17976, 17978, 20028, 20367, 20418, 20829, 20834, 20986, 21020, 21045, 21103, 21140, 21147, 21287, 21336, 21418, 21496, 21530, 21725, 21877, 22205, 22467, 22880, 22924, 22925, 22971, 23118, 23547)

INSERT INTO url_html (url_id, html, html_js)
(SELECT id as url_id, html, htmljs as html_js
FROM urls);

INSERT INTO url_markup (url_id, markup, elem_count)
(SELECT id as url_id, urls.markup, urls.elem_count
FROM urls);

UPDATE urls SET has_html=True WHERE html != '';
UPDATE urls SET has_markup=True WHERE markup is not null;


UPDATE urls SET text_approved=False WHERE text_approved=True;

UPDATE urls set cat_id=16 where cat_id=22;

SELECT url_id, elem_count FROM url_markup WHERE elem_count > 5 ORDER BY elem_count DESC ;

SELECT count(id) FROM url_markup WHERE elem_count >1;

SELECT status_code, count(id) FROM urls where load_err is true GROUP BY status_code ;

SELECT status_code, count(id) FROM urls WHERE id in
(SELECT url_id FROM url_markup WHERE elem_count > 0 AND elem_count < 10)
GROUP BY status_code ;

UPDATE urls SET deleted=True WHERE status_code != 200 AND id in (
SELECT url_id FROM url_markup WHERE elem_count > 0 AND elem_count < 10
);



DELETE FROM urls WHERE id in (SELECT url_id FROM url_markup WHERE elem_count > 1000);


SELECT count(id) from urls where deleted=True;

SELECT count(id) from urls where load_err=True;

SELECT count(id) from urls where has_markup=False;

DELETE from urls where deleted=True;

SELECT count(id) FROM url_markup WHERE elem_count > 0 AND markup is null;


SELECT url_id, markup, elem_count FROM url_markup  INNER JOIN urls u on url_markup.url_id = u.id
WHERE u.deleted != True AND elem_count > 0 AND elem_count < 15 ORDER BY elem_count DESC ;

UPDATE urls SET load_err=True WHERE id in (
SELECT url_id FROM url_markup WHERE elem_count > 0 AND markup is null
);


SELECT id, cat_id, url FROM urls   WHERE has_markup=False AND load_err=True ORDER BY id;

SELECT urls.id, cat_id, url FROM urls
            LEFT JOIN url_html uh on urls.id = uh.url_id
            LEFT  JOIN url_markup um on urls.id = um.url_id
            WHERE urls.id >= 0 AND (uh.html_js='' OR uh.html_js IS NULL OR um.markup IS NULL)
            ORDER BY urls.id;

UPDATE urls SET load_err=False, has_markup=False where load_err=True;

UPDATE urls set text_approved=True WHERE lang='ru';








SELECT
  x.id, x.cat_id, c.name, x.url, ut.lemmas
FROM (
  SELECT
    ROW_NUMBER() OVER (PARTITION BY cat_id ORDER BY id) AS r,
    u.* FROM urls u) x
LEFT JOIN categories c ON c.id=x.cat_id
INNER JOIN url_texts ut ON x.id = ut.url_id
WHERE
  x.r <= 500
ORDER BY cat_id;


SELECT u.cat_id, count(cat_id) FROM url_texts LEFT JOIN urls u on url_texts.url_id = u.id
WHERE lemmas_count > 5000 GROUP BY u.cat_id;


DELETE FROM urls where id in (SELECT url_id FROM url_texts WHERE lemmas_count > 5000)



SELECT url_id, lemmas, lemmas_count FROM url_texts ORDER BY lemmas_count ASC;



SELECT url_id, lemmas  FROM url_texts WHERE lemmas::jsonb::text  ilike '%http%';


SELECT setval('detected_categories_id_seq', (SELECT MAX(id) FROM detected_categories)+1);


