app:
	docker-compose exec app bash
cli:
	docker-compose run --rm queue bash
queue:
	docker-compose run --rm queue sh -c 'rq worker markup_tasks -u ${REDIS_URL}'
redis:
	docker-compose exec redis sh
yinit:
	docker-compose run --rm node yarn install
yrun:
	docker-compose run --rm node yarn run dev
yprod:
	docker-compose run --rm node yarn run prod
yhot:
	docker-compose run --rm node yarn run hot
ylive:
	docker-compose run --rm node yarn run watch
