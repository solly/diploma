create table categories
(
	id serial not null
		constraint catalog_cm_pk
			primary key,
	name varchar
);

alter table categories owner to pgdev;


create table detected_categories
(
	id serial not null
		constraint detected_categories_pk
			primary key,
	name varchar
);

alter table detected_categories owner to pgdev;

create table urls
(
	id serial not null
		constraint url_content_pk
			primary key,
	cat_id integer
		constraint urls_categories_id_fk
			references categories
				on update set null on delete set null,
	url varchar(1024) not null,
	host varchar,
	charset varchar default 'utf8'::character varying,
	is_main boolean,
	load_err boolean default false,
	status_code integer,
	has_html boolean default false not null,
	has_markup boolean default false not null,
	lang varchar(50) default 'ru'::character varying not null,
	deleted boolean default false not null,
	category_id integer,
	layout integer default 0,
	bad_image boolean default false
);

alter table urls owner to pgdev;

create unique index url_content_url_uindex
	on urls (url);

create table url_texts
(
	id serial not null
		constraint url_text_pk
			primary key,
	url_id integer
		constraint url_text_url_id_fk
			references urls
				on update cascade on delete cascade,
	title text,
	raw_text text,
	meta_keys varchar(2048) default ' '::character varying not null,
	meta_desc varchar(2048) default ''::character varying not null,
	lemmas jsonb,
	lemmas_count integer default 0 not null
);

alter table url_texts owner to pgdev;

create unique index url_text_url_id_uindex
	on url_texts (url_id);

create table url_html
(
	id bigserial not null
		constraint url_html_pkey
			primary key,
	url_id bigint not null
		constraint url_html_url_id_foreign
			references urls
				on delete cascade,
	html text,
	html_js text
);

alter table url_html owner to pgdev;

create unique index url_html_url_id_uindex
	on url_html (url_id);

create table url_markup
(
	id bigserial not null
		constraint url_markup_pkey
			primary key,
	url_id bigint not null
		constraint url_markup_url_id_foreign
			references urls
				on delete cascade,
	markup jsonb,
	elem_count integer default 0,
	train_markup jsonb,
	max_depth integer,
	height integer
);

alter table url_markup owner to pgdev;

create unique index url_markup_url_id_uindex
	on url_markup (url_id);


create table tasks
(
	id varchar not null
		constraint tasks_pk
			primary key,
	name varchar,
	payload jsonb,
	complete boolean default false
);

alter table tasks owner to pgdev;

