const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
//mix.setPublicPath('app/static/');
//mix.setResourceRoot('./');

mix.js('app/resources/js/app.js', 'app/static')
    .sass('app/resources/sass/app.scss', 'app/static')
;
if (mix.inProduction()) {
    mix.version();
} else {
    mix.sourceMaps();
}
