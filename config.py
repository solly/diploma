import os
from dotenv import load_dotenv
load_dotenv()


class Config(object):
    CSRF_ENABLED = False
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    SECRET_KEY = 'ltcznmcbyb[j,tpmzyd;jgeceyekb,fyfy'
    JSON_AS_ASCII = False
    JSON_SORT_KEYS = True
    REDIS_URL = os.getenv('REDIS_URL')
    PONY = {
        'provider': 'postgres',
        'user': os.getenv('DB_USER'),
        'password': os.getenv('DB_PASS'),
        'host': os.getenv('DB_HOST'),
        'port': os.getenv('DB_PORT'),
        'database': os.getenv('DB_NAME'),
    }