import sys
from rq import get_current_job
from app.models import *
from lib.browsery import load_one, screen_one
from lib.utils.html_process import extract_text_title_meta, train_markup, markup_max_depth, markup_max_visual_height
from lib.utils.text_process import lemmatize_morphy, detect_lang
from app import create_app

app = create_app()
app.app_context().push()


@db_session
def _set_task_progress(progress):
    job = get_current_job()
    if job:
        job.meta['progress'] = progress
        job.save_meta()
        task = Task[job.get_id()]
        if progress >= 100:
            task.complete = True
        commit()


def update_screen(url_id: int):
    _set_task_progress(5)
    screen_one(url_id)
    _set_task_progress(100)


@db_session
def update_data(url_id: int):
    _set_task_progress(5)
    load_one(url_id)
    _set_task_progress(50)
    url = Url[url_id]
    url.markup.max_depth = markup_max_depth(url.markup.markup)
    url.markup.height = markup_max_visual_height(url.markup.markup)
    url.markup.train_markup = train_markup(url.markup.markup)
    commit()
    _set_task_progress(80)
    title, meta, text = extract_text_title_meta(url.html.html_js)
    url.text.title = title
    url.text.raw_text = text
    url.text.meta_keys = meta['keys']
    url.text.meta_desc = meta['desc']
    url.text.lemmas = lemmatize_morphy(text)
    url.text.lemmas_count = len(url.text.lemmas)
    commit()
    _set_task_progress(100)