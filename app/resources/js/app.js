require('./bootstrap');
import Zooming from 'zooming';
import Vue from 'vue';
import Markup from "./components/Markup";
import vmodal from 'vue-js-modal';

new Zooming({enableGrab: true, scaleBase: 2.0, customSize: '100%',}).listen('img.zoomable');

$(document).ready(function () {
    $(document).on('click', 'a.atoggle', function (e) {
        let val = $(this).data('val');
        let url = $(this).data('url');
        let el = $(this);
        $.post(url).done(function () {
            if (val === 'true') {
                el.html('<i class="fas fa-circle"></i>').data('val', 'false')
                    .removeClass('text-success').addClass('text-danger');
            } else {
                el.html('<i class="fas fa-check-circle"></i>').data('val', 'true')
                    .removeClass('text-danger').addClass('text-success');
            }
        });
    });
    $(document).on('click', 'a.expand_img', function (e) {
        e.preventDefault();
        $('#imagepreview').attr('src', $(this).data('src'));
        $('#imagemodal').modal('show');
    });
    $(document).on('click', 'button.btn-ajax', function (e) {
        e.preventDefault();
        let btn = $(this)
        let form = $(this).closest('form');
        $.post(form.attr('action'), form.serialize()).done(function (resp) {
            console.info(resp.data);
            btn.removeClass('btn-secondary').addClass('btn-success');
        }).fail(function (resp) {
            console.error(resp);
            btn.removeClass('btn-secondary').addClass('btn-danger');
        });
    });
});

Vue.use(vmodal);

const vm = new Vue({
    el: '#markuper',
    components: {'markup': Markup},
});