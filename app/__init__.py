from flask import Flask, current_app
from pony.flask import Pony
from pony.orm import Database
from flask_debugtoolbar import DebugToolbarExtension
from flask_fontawesome import FontAwesome
from redis import Redis
from config import Config
import rq

db = Database()
pony = Pony()
toolbar = DebugToolbarExtension()

from . import models
from .views import common


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)
    app.debug = False
    db.bind(**app.config['PONY'])
    pony.init_app(app)
    db.generate_mapping(create_tables=False)
    app.redis = Redis.from_url(app.config['REDIS_URL'])
    app.task_queue = rq.Queue('markup_tasks', connection=app.redis)
    app.register_blueprint(common)
    FontAwesome(app)
    toolbar.init_app(app)
    return app


from . import views