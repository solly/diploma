from flask import render_template, redirect, flash, request, jsonify, send_from_directory, abort, current_app, Blueprint
from .forms import UrlTextApproveForm, LayoutForm
from app.models import *
from flask_csp.csp import csp_header
from lib.utils.utils import delete_url_screens, move_url_screens
from lib.utils.html_process import extract_text_title_meta
from math import floor
from redis import Redis
import rq
import json
import os

common = Blueprint(
    'common', __name__,
    template_folder='templates',
    static_folder='static',
    static_url_path='/static',
)


@common.route('/favicon.ico', methods=['GET'])
def favicon():
    return send_from_directory(os.path.join(current_app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


@csp_header({"default-src": "'self'", "script-src": "'self'", "worker-src": "'self'"})
@common.route('/')
@common.route('/index')
def index():
    categories = select((u.cat, count(u.id)) for u in Url if u.deleted is False and u.cat)[:]
    with_markup = select((u.cat.id, count(u.id)) for u in Url if u.has_markup is True and u.deleted is False)[:]
    with_markup = {i[0]: i[1] for i in with_markup}
    return render_template("index.html", categories=categories, with_markup=with_markup)


@common.route('/old_cat')
def old_index():
    categories = select((u.old_cat, count(u.id)) for u in Url if u.deleted is False)[:]
    return render_template("old_index.html", categories=categories)


@common.route('/oldcat/<int:idx>', methods=['GET'], defaults={'page': 1})
@common.route('/oldcat/<int:idx>/page/<int:page>', methods=['GET'])
def old_cat(idx: int, page: int = 1):
    category = OldCategory[idx]
    if not category:
        abort(404)
    limit = int(request.args.get('limit', type=int, default=30))
    page = page if page > 0 else 1
    limit = limit if 0 < limit < 1000 else 30
    sort = request.args.get('sort', type=str, default='id')
    dir = request.args.get('dir', type=str, default='asc')
    search = request.args.get('search', type=str, default='')
    if not search.isalnum():
        search = ''
    if sort not in ['lang', 'is_main', 'url', 'host']:
        sort = 'id'
    order_by = getattr(Url, sort)
    if dir == 'desc':
        order_by = desc(order_by)
    query = category.urls.select(lambda u: u.deleted is False)
    if len(search) > 2:
        query = query.filter(lambda u: search in u.text.raw_text)
    urls = query.order_by(order_by).page(page, limit if limit > 0 else 30)
    total = query.count()
    max_page = floor(total / limit) + 1
    return render_template('cat.html',
                           cat=category,
                           route='common.old_cat',
                           urls=urls,
                           sort=sort,
                           dir=dir,
                           max_page=max_page,
                           page=page,
                           search=search
                           )


@common.route('/cat/common/<int:idx>', methods=['GET'], defaults={'page': 1})
@common.route('/cat/common/<int:idx>/page/<int:page>', methods=['GET'])
def cat(idx: int, page: int = 1):
    category = Category[idx]
    if not category:
        abort(404)
    limit = int(request.args.get('limit', type=int, default=30))
    page = page if page > 0 else 1
    limit = limit if 0 < limit < 1000 else 30
    sort = request.args.get('sort', type=str, default='id')
    dir = request.args.get('dir', type=str, default='asc')
    search = request.args.get('search', type=str, default='')
    if not search.isalnum():
        search = ''
    if sort not in ['lang', 'is_main', 'url', 'host']:
        sort = 'id'
    order_by = getattr(Url, sort)
    if dir == 'desc':
        order_by = desc(order_by)
    query = category.urls.select(lambda u: u.deleted is False)
    if len(search) > 2:
        query = query.filter(lambda u: search in u.text.raw_text)
    urls = query.order_by(order_by).page(page, limit if limit > 0 else 30)
    total = query.count()
    max_page = floor(total / limit) + 1

    return render_template('cat.html',
                           cat=category,
                           route='common.cat',
                           urls=urls,
                           sort=sort,
                           dir=dir,
                           max_page=max_page,
                           page=page,
                           search=search
                           )


@common.route('/images', methods=['GET'], defaults={'page': 1})
@common.route('/images/page/<int:page>', methods=['GET'])
def image_index(page: int = 1):
    limit = int(request.args.get('limit', type=int, default=100))
    page = page if page > 0 else 1
    limit = limit if 0 < limit < 1000 else 100
    sort = request.args.get('sort', type=str, default='layout')
    dir = request.args.get('dir', type=str, default='desc')
    search = request.args.get('search', type=str, default='')
    if not search.isalnum():
        search = ''
    if sort not in ['lang', 'is_main', 'url', 'host', 'cat', 'old_cat', 'id', 'layout']:
        sort = 'layout'
    order_by = getattr(Url, sort)
    if dir != 'asc':
        order_by = desc(order_by)
    query = Url.select(lambda u: u.deleted is False)
    if len(search) > 2:
        query = query.filter(lambda u: search in u.text.title or search in u.text.raw_text)
    urls = query.order_by(order_by).page(page, limit)
    total = query.count()
    max_page = floor(total / limit) + 1
    forms = {}
    for url in urls:
        forms[url.id] = LayoutForm(obj=url)
    return render_template('images.html',
                           route='common.image_index',
                           urls=urls,
                           sort=sort,
                           dir=dir,
                           max_page=max_page,
                           page=page,
                           forms=forms,
                           search=search
                           )


@common.route('/url/<int:url_id>', methods=['GET', 'POST'])
def show(url_id: int):
    url = Url[url_id]
    if not url:
        abort(404)
    current_cat = url.cat
    categories = select((c.id, c.name) for c in OldCategory)[:]
    form = UrlTextApproveForm(obj=url)
    form.cat.choices = categories
    if request.method == 'GET':
        form.cat.data = url.cat.id
    if form.validate_on_submit():
        with db_session:
            url.cat = OldCategory[form.cat.data]
            url.lang = form.lang.data
            url.is_main = form.is_main.data
            url.deleted = form.deleted.data
            commit()
        flash('Изменения сохранены %s' % form.cat.data, 'success')
        if current_cat != url.cat:
            move_url_screens(url.id, current_cat.id, url.cat.id)
            flash('Скрины перемещены', 'success')
        next = Url.select(lambda u: u.id > url.id and u.cat == current_cat).order_by(Url.id).first()
        if next:
            return redirect('/url/%s' % next.id)
        return redirect('/url/%s' % url.id)
    else:
        next = Url.select(lambda u: u.id > url.id and u.cat == current_cat).order_by(Url.id).first()
        prev = Url.select(lambda u: u.id < url.id and u.cat == current_cat).order_by(desc(Url.id)).first()
        return render_template('url.html', url=url, next=next, prev=prev, form=form)


@csp_header({"default-src": "'self'", "script-src": "'self'", "worker-src": "'self'"})
@common.route('/markup/<int:url_id>', methods=['GET', 'POST'])
def markup(url_id: int):
    url = Url[url_id]
    current_cat = url.cat
    if request.method == 'POST':
        train_markup = request.form.get('markup')
        return jsonify({'success': True, 'data': train_markup})
    markup = json.dumps(url.markup.markup)
    next = Url.select(lambda u: u.id > url.id and u.cat == current_cat).order_by(Url.id).first()
    prev = Url.select(lambda u: u.id < url.id and u.cat == current_cat).order_by(desc(Url.id)).first()
    return render_template('markup.html', url=url, markup=markup, next=next, prev=prev)


@db_session
@common.route('/url/toggle/<int:url_id>/attr/<attr>', methods=['POST', 'AJAX'])
def toggle_attr(url_id: int, attr: str):
    url = Url[url_id]
    if not url or attr not in ['lang', 'is_main', 'deleted', 'bad_image']:
        abort(404)
    if attr == 'lang':
        url.lang = 'ru'
    else:
        val = getattr(url, attr)
        setattr(url, attr, not val)
    commit()
    return jsonify({'success': True, 'attr': attr, 'new val': getattr(url, attr)})


@db_session
@common.route('/url/layout/<int:url_id>', methods=['POST'])
def update_layout(url_id: int):
    url = Url[url_id]
    if not url:
        abort(404)
    form = LayoutForm(obj=url)
    if form.validate_on_submit():
        url.layout = form.layout.data
        commit()
    return jsonify({'success': True, 'layout': url.layout})


@db_session
@common.route('/url/text-refresh/<int:url_id>', methods=['GET'])
def update_text(url_id: int):
    try:
        url = Url[url_id]
        if not url:
            abort(404)
        title, meta, text = extract_text_title_meta(url.html.html_js)
        url.text.title = title
        url.text.raw_text = text
        url.text.meta_keys = meta['keys']
        url.text.meta_desc = meta['desc']
        commit()
        flash('text has been updated', 'success')
    except Exception:
        flash('fail text refresh', 'error')
    return redirect('/url/%s' % url_id)


@db_session
@common.route('/url/screen-refresh/<int:url_id>', methods=['GET', 'POST'])
def update_screen(url_id: int):
    url = Url[url_id]
    if not url:
        abort(404)
    rq_job = current_app.task_queue.enqueue('app.tasks.update_screen', url_id)
    task = Task(
        id=rq_job.get_id(),
        name='update_screen',
        payload={'url_id': url_id}
    )
    commit()
    if request.method == 'GET':
        flash(f'task {task.id} was queued', 'success')
        return redirect('/url/%s' % url_id)
    else:
        return jsonify({'success': True, 'task': task.id})

@db_session
@common.route('/url/data-refresh/<int:url_id>', methods=['GET', 'POST'])
def update_data(url_id: int):
    url = Url[url_id]
    if not url:
        abort(404)
    rq_job = current_app.task_queue.enqueue('app.tasks.update_data', url_id)
    task = Task(
        id=rq_job.get_id(),
        name='update_data',
        payload={'url_id': url_id}
    )
    commit()
    if request.method == 'GET':
        flash(f'task {task.id} was queued', 'success')
        return redirect('/url/%s' % url_id)
    else:
        return jsonify({'success': True, 'task': task.id})

@db_session
@common.route('/url/delete/<int:url_id>', methods=['POST'])
def delete_url(url_id: int):
    url = Url[url_id]
    if not url:
        abort(404)
    url.deleted = True
    url.html.html = ''
    url.html.html_js = ''
    url.text.title = ''
    url.text.raw_text = ''
    commit()
    delete_url_screens(url.id, url.cat.id)

    next = Url.select(lambda u: u.id > url.id and u.cat == url.cat).order_by(Url.id).first()
    if next:
        return redirect('/url/%s' % next.id)
    prev = Url.select(lambda u: u.id < url.id and u.cat == url.cat).order_by(desc(Url.id)).first()
    return redirect('/url/%s' % prev.id)


@common.route('/cat/markup/<int:idx>', methods=['GET'])
def cat_markup(idx, since=None):
    pass
# category = db.query_one('SELECT * FROM categories WHERE id=%s', (int(idx),))
# return render_template('cat.html', category=category)
