from pony.orm import *
from app import db
from flask import current_app
import redis
import rq


class OldCategory(db.Entity):
    _table_ = 'categories'
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    urls = Set('Url', cascade_delete=False)


class Category(db.Entity):
    _table_ = 'detected_categories'
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    urls = Set('Url', cascade_delete=False)


class Url(db.Entity):
    _table_ = 'urls'
    id = PrimaryKey(int, auto=True)
    old_cat = Required('OldCategory', column='cat_id')
    cat = Required('Category', column='category_id')
    url = Required(str)
    host = Required(str)
    charset = Required(str)
    lang = Optional(str, default='UNKNOWN')
    layout = Optional(int, default=0)
    is_main = Required(bool, default=True)
    load_err = Required(bool, default=False)
    status_code = Optional(int)
    has_html = Required(bool, default=False)
    has_markup = Required(bool, default=False)
    bad_image = Required(bool, default=False)
    deleted = Required(bool, default=False)
    text = Optional('UrlText')
    html = Optional('UrlHtml')
    markup = Optional('UrlMarkup')

    def get_thumb_url(self):
        return '/static/htmldb/%s/%s_sm.jpg' % (self.old_cat.id, self.id)

    def get_image_url(self):
        return '/static/htmldb/%s/%s.jpg' % (self.old_cat.id, self.id)


class UrlText(db.Entity):
    _table_ = 'url_texts'
    id = PrimaryKey(int, auto=True)
    url = Required('Url', column='url_id')
    title = Optional(str, default='', sql_type='text')
    meta_keys = Optional(str, default='')
    meta_desc = Optional(str, default='')
    raw_text = Optional(str, default='', sql_type='text')
    lemmas = Optional(Json, default='', sql_type='text')
    lemmas_count = Optional(int, default=0)


class UrlHtml(db.Entity):
    _table_ = 'url_html'
    id = PrimaryKey(int, auto=True)
    url = Required('Url', column='url_id')
    html = Optional(str, default=None, sql_type='text', nullable=True)
    html_js = Optional(str, default=None, sql_type='text', nullable=True)


class UrlMarkup(db.Entity):
    _table_ = 'url_markup'
    id = PrimaryKey(int, auto=True)
    url = Required('Url', column='url_id')
    elem_count = Optional(int, default=0)
    markup = Optional(Json, default=None, nullable=True)
    train_markup = Optional(Json, default=None, nullable=True)
    max_depth = Optional(int, default=0)
    height = Optional(int, default=0)


class Task(db.Entity):
    _table_ = 'tasks'
    id = PrimaryKey(str, auto=False)
    name = Required(str)
    payload = Required(Json, nullable=True)
    complete = Optional(bool, default=False)

    def get_rq_job(self):
        try:
            rq_job = rq.job.Job.fetch(self.id, connection=current_app.redis)
        except (redis.exceptions.RedisError, rq.exceptions.NoSuchJobError):
            return None
        return rq_job

    def get_progress(self):
        job = self.get_rq_job()
        return job.meta.get('progress', 0) if job is not None else 100


