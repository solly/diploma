from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, PasswordField, SelectField, SubmitField
from wtforms.validators import DataRequired, Optional, InputRequired

LAYOUTS = [
    (0, 'Undefined'),
    (1, 'Classic Landing'),
    (2, 'left+content+right'),
    (3, 'left + content'),
    (4, 'Blog (content + right)'),
    (5, 'Promo - minimal text content, big image'),
    (6, 'Simple (content without columns, inc. error pages, sitemaps)'),
    (7, 'MultiColumn/Tile/Complex Landing'),
    (8, 'Product catalog'),
    (9, 'Forum'),
    (10, 'Old style main page'),
    (11, 'Exclusive design'),
]


class LoginForm(FlaskForm):
    login = StringField('login', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    remember_me = BooleanField('remember_me', default=False)


class ChangeCategory(FlaskForm):
    cat = SelectField('cat', coerce=int, validators=[DataRequired()])


class LayoutForm(FlaskForm):
    layout = SelectField('layout', coerce=int, validators=[InputRequired()], choices=LAYOUTS)


class UrlTextApproveForm(FlaskForm):
    cat = SelectField('cat.id', coerce=int, validators=[DataRequired()])
    lang = StringField('lang', validators=[DataRequired()])
    is_main = BooleanField('is_main', default=False)
    deleted = BooleanField('deleted',   default=False)
    submit = SubmitField('Submit')
