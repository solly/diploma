#!/usr/bin/env python
"""
 Load urls with real chrome-headless browser. Traverse across DOM and build html-markup for visible elements with data
   - tagName,
   - xpath,
   - parent element xpath,
   - level of tag depth,
   - number of children elements,
   - position coordinates,
   - common element attributes
   - text value (if exists)

  :Usage: python -m lib.browsery task (start_id (chunk_size (max_tasks)))
         [Required: task - new|retry|bad]
         [Optional: start_id: int]
         [Optional: chunk_size: int]
         [Optional: max_tasks : int]
"""

import asyncio
import os
import sys
import logging
import signal

from pyppeteer import launch
from pyppeteer.browser import Browser
from pyppeteer.page import PageError
from pyppeteer.errors import TimeoutError, NetworkError
import psycopg2
from .utils.db import Db
from .utils.html_process import minify
from .utils.utils import LOG_DIR, screen_path, save_html, save_json

sys.setrecursionlimit(10 ** 6)
db = Db()
logging.basicConfig(filename=os.path.join(LOG_DIR, 'browser_loader.log'), level=logging.INFO)
logging.captureWarnings(True)

JS = """
() => {
const ignoredNodes = ["NOSCRIPT", "SCRIPT", "STYLE", "NOINDEX", "NOTYPOGRAF"];
const allowedAttrs = [
'src','alt','title','href','name','class','src','type','content','value','visibility', 'lang', 'rel', 'srcset',
 'source', 'width', 'height', 'size', 'language', 'action'
];
let nodeList = []
function xpathFor(element) {
    const idx = (sibling, name) => sibling 
        ? idx(sibling.previousElementSibling, name||sibling.localName) + (sibling.localName == name)
        : 1;
    const segments = function(el){
      if(!el || (el.nodeType !== 1 && el.nodeType !== 3)){ return ['']; }
      //if(el.id && document.getElementById(el.id) === el){ return [`id('${el.id}')`]; }
      let current = el.nodeType === 3? 'text()': el.localName.toLowerCase();
      if (el.nodeType !== 3 && ! ['html', 'body', 'head'].includes(current) ){
         current += `[${idx(el)}]`;
      }
      return [...segments(el.parentNode), current];
    }
    result = segments(element);
    return [result.join('/'), result.length -2];
}
function attributesFor(element){
   let attrs = {}
   if(!element.hasAttributes()){
      return attrs;
   }
   let attrMap = element.attributes
   for (let i = 0; i < attrMap.length; i++){
      let item = attrMap[i];
      let itemName = item.name.toLowerCase();
      if (allowedAttrs.includes(itemName)){
          attrs[itemName] = item.value
      }
   }
   return attrs;
}
function rectFor(element){
   r = element.getClientRects()[0]||null;
   return r? {"x": Math.round(r.x), "y": Math.round(r.y), "x1": Math.round(r.x + r.width), "y1": Math.round(r.y + r.height)}: null;
}
function textsFor(element){
   let texts=[];
   element.childNodes.forEach(function(el){
        if(el.nodeType === 3){
           let value = el.nodeValue.replace(/(\\r\\n|\\n|\\r)/gm, '').replace('  ', '').trim();
           if (value.length){texts.push(value);}
        }
   });
   return texts.join(' ');
}
function nodeInfo(el, parent){
    if (! el){ return null;}
    const rect = rectFor(el);
    const [xpath, level] = xpathFor(el);
    if (xpath.includes('/body/') && rect === null) { return null;} <!-- invisible element -->
    const name = el.nodeName.toLowerCase();
    <!-- avoid inconsistent tag detection -->
    if( !el.nodeName.match(/^[\w\d-_:]+$/igm)){
       return null;
    }
    let info = {
       "tag": name,
       "id": xpath,
       "l": level,
       "cnt": el.childNodes.length,
       "x": rect['x'],
       "y": rect['y'],
       "x1": rect['x1'],
       "y1": rect['y1'],
       "v": textsFor(el),
      // "atr": attributesFor(el)
       "mark":""
    }
    return info;
}

function addToMap(level, info){
   if( ! nodeMap.hasOwnProperty(level)){
      nodeMap[level] = [];
   }
   if(info !== null){
      nodeMap[level].push(info);
   }
}

function scanNode(node, parent){
    const info = nodeInfo(node, parent);
    if(info !== null){
       nodeList.push(info)
       if(node.childNodes.length && info['name'] !== 'svg'){
          node.childNodes.forEach(function(el){
             let validNode = el && el.nodeName && el.nodeType === 1 && !ignoredNodes.includes(el.nodeName);
             if(validNode && el.nodeName.match(/[\w\d-_]+/i)){
                 scanNode(el, info['xpath']);
             }
          });
       }
    }
}

function getNodeList(){
   <!-- scanNode(document.querySelector('head'), '/html'); -->
   scanNode(document.querySelector('body'), '/html');
   return nodeList;
}
return getNodeList();
}
"""


def patch_pyppeteer():
    import pyppeteer.connection
    original_method = pyppeteer.connection.websockets.client.connect

    def new_method(*args, **kwargs):
        kwargs['ping_interval'] = None
        kwargs['ping_timeout'] = None
        return original_method(*args, **kwargs)

    pyppeteer.connection.websockets.client.connect = new_method


patch_pyppeteer()


async def run_browser():
    args = ["--no-sandbox", "--disable-setuid-sandbox", "--ignore-certificate-errors"]
    browser = await launch({
        'ignoreHTTPSErrors': True,
        'headless': True,
        'args': args,
        'userDataDir': os.path.abspath('../tmp/browser_data/')
    })
    return browser


async def make_screen(browser: Browser, url: str, screen_path: str):
    page = await browser.newPage()
    await page.setJavaScriptEnabled(True)
    await page.setCacheEnabled(True)
    await page.setUserAgent(
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36')
    await page.setViewport({'width': 1024, 'height': 768})
    try:
        response = await page.goto(url, {'waitUntil': ['load', 'networkidle2']})
    except (PageError, TimeoutError) as e:
        await page.close()
        raise Exception(str(e))
    await page.evaluateOnNewDocument('''
    navigator.geolocation.getCurrentPosition = function(success, failure) {
               success({ coords: {latitude: 31.230416,longitude: 121.473701}, timestamp: Date.now() });
         };
    ''')
    await page.waitFor(1000)
    await page.evaluate('''window.scrollBy(0, window.innerHeight);''')
    try:
       await page.click('body')
    except:
        pass
    await page.screenshot(
        {'path': screen_path.replace('.jpg', '_sm.jpg'), 'fullPage': False, 'type': 'jpeg', 'quality': 65})
    await page.screenshot({'path': screen_path, 'fullPage': True, 'type': 'jpeg', 'quality': 75})
    await page.close()


async def load_url(browser: Browser, url: str, screen_path: str, retry: int = 0):
    page = await browser.newPage()
    await page.setJavaScriptEnabled(True)
    await page.setCacheEnabled(True)
    await page.setUserAgent(
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36')
    await page.setViewport({'width': 1024, 'height': 768})
    try:
        response = await page.goto(url, {'waitUntil': ['load', 'networkidle2', 'networkidle0']})
    except (PageError, TimeoutError) as e:
        await page.close()
        raise Exception(str(e))
    if response:
        print('    - loaded', url, response.status)
    else:
        print('    - no response', url)

    await page.evaluateOnNewDocument('''
        navigator.geolocation.getCurrentPosition = function(success, failure) {
                   success({ coords: {latitude: 31.230416,longitude: 121.473701}, timestamp: Date.now() });
             };
        ''')
    await page.waitFor(500)
    await page.click('body')
    await page.screenshot(
        {'path': screen_path.replace('.jpg', '_sm.jpg'), 'fullPage': False, 'type': 'jpeg', 'quality': 65})
    await page.screenshot({'path': screen_path, 'fullPage': True, 'type': 'jpeg', 'quality': 75})
    print('    - screened', url)
    content = await page.content()
    nodes = await page.evaluate(JS, force_expr=False)
    print('    - evaluated', url)
    await page.close()
    return content, nodes, response.status


async def process_item(item: dict, browser: Browser):
    html = None
    nodes = None
    status = None
    print(item)
    try:
        html, nodes, status = await load_url(browser, item['url'], screen_path(item['id'], item['cat_id']))
    except Exception as e:
        print(e)
        logging.exception('Url load exception %s ' % item['url'])
        pass
    if html and nodes:
        html_min = minify(html.replace('\x00', '').replace('\u0000', ''))
        try:
            db.save_browser_content(item['id'], html_min, nodes, status)
            print('    - saved', item['url'])
        except psycopg2.errors.UntranslatableCharacter:
            print('Fail bad characters in url', item['url'])
            db.mark_load_error(item['id'])
        except Exception as e:
            logging.exception('Url save exception')
            print('Fail save url', item['url'])
            raise e
    else:
        db.mark_load_error(item['id'])


async def load(query, start_id: int = 0, limit: int = 50, max_tasks: int = 1000):
    print('Start portion since id =', start_id)
    items = query(start_id, limit)
    i = 0
    browser = await run_browser()
    while len(items) > 0 and i < max_tasks:
        i += 1
        if browser is None:
            browser = await run_browser()
        start_id = max([item['id'] for item in items])
        tasks = [asyncio.create_task(process_item(item, browser)) for item in items]
        await asyncio.wait(tasks, timeout=75)
        print('tasks %s complete next since %s' % (i, start_id))
        if browser is not None and i % 20 == 0:
            await browser.close()
            browser = None
        items = query(start_id, limit)

    if browser is not None:
        await browser.close()


def load_one(url_id: int):
    item = db.query_one('''SELECT id, cat_id, category_id, url FROM urls where id=%s''', (url_id,))
    loop = create_loop()

    async def task():
        browser = await run_browser()
        await process_item(item, browser)
        await browser.close()

    loop.run_until_complete(task())


def screen_one(url_id: int):
    item = db.query_one('''SELECT id, cat_id, category_id, url FROM urls where id=%s''', (url_id,))
    loop = create_loop()

    async def task():
        browser = await run_browser()
        await make_screen(browser, item['url'], screen_path(item['id'], item['cat_id']))
        await browser.close()

    loop.run_until_complete(task())


async def shutdown(loop, signal=None):
    """Cleanup tasks tied to the service's shutdown."""
    if signal:
        logging.info(f"Received exit signal {signal.name}...")
    logging.info("Shutdown")


def handle_exception(loop, context):
    msg = context.get("exception", context["message"])
    logging.error(f"Caught exception: {msg}")
    logging.info("Shutting down...")
    asyncio.create_task(shutdown(loop))


def create_loop():
    loop = asyncio.get_event_loop()
    signals = (signal.SIGHUP, signal.SIGTERM, signal.SIGINT)
    for s in signals:
        loop.add_signal_handler(
            s, lambda s=s: asyncio.create_task(shutdown(loop, signal=s)))
    loop.set_exception_handler(handle_exception)
    return loop


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Invalid usage')
    op = sys.argv[1]
    if op == 'one':
        id = sys.argv[2]
        load_one(int(id))
    else:
        start_id = sys.argv[2] if len(sys.argv) > 2 else 0
        limit = sys.argv[3] if len(sys.argv) > 3 else 20
        max_tasks = sys.argv[4] if len(sys.argv) > 4 else 1000
        if op == 'new':
            def query(start, lim):
                return db.get_content_not_loaded_urls(start, lim)
        elif op == 'retry':
            def query(start, lim):
                return db.get_content_err_loaded_urls(start, lim)
        elif op == 'bad':
            def query(start, lim):
                return db.get_content_bad_loaded_urls(start, lim)
        else:
            def query(start, lim):
                return []
        loop = create_loop()
        loop.run_until_complete(load(query, int(start_id), int(limit), int(max_tasks)))
