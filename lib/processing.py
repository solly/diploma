#!/usr/bin/env python

"""
Tools for analyze url data and some additional operations
"""

import os
import sys
import json
import re
from .utils.db import Db
from .utils.html_process import extract_text_title_meta, train_markup, markup_max_depth, markup_max_visual_height
from .utils.text_process import lemmatize_morphy, detect_lang
from .utils.utils import TEMP_DIR, SCREEN_DIR, delete_url_screens, move_url_screens
from math import floor

db = Db()
sys.setrecursionlimit(10 ** 6)
VALID_TAG = re.compile('^[\w\d\-_:]+$', re.IGNORECASE | re.UNICODE)


def markup_for_train(start_id: int = 0, limit: int = 100, max_num: int = 40000, total: int = 0):
    '''
    Process markup to list of lists where each list contains ordered:
      [xpath, tag,level, child_count, x,y,x1,y1, value length, mark]
    :param start_id:
    :param limit:
    :return:
    '''
    if total > max_num:
        exit(0)
    print('Start portions since id ', start_id)
    items = db.get_url_markup(start_id, limit)
    if len(items) == 0:
        exit(0)
    total += len(items)
    for item in items:
        if item['id'] > start_id:
            start_id = item['id']
        max_depth = markup_max_depth(item['markup'])
        max_height = markup_max_visual_height(item['markup'])
        markup = train_markup(item['markup'])
        if len(markup):
            db.save_train_markup(item['id'], markup, max_depth, max_height)
    markup_for_train(start_id, limit, max_num, total)


def lemmatize(start_id: int = 0, limit: int = 100, max_num: int = 30000, total: int = 0):
    if total > max_num:
        exit(0)
    print('Start portions since id ', start_id)
    items = db.get_url_texts(start_id, limit)
    if len(items) == 0:
        exit(0)
    total += len(items)
    for item in items:
        if item['id'] > start_id:
            start_id = item['id']
        text = (item['raw_text'] or '').strip()
        if text != '':
            title = (item['title'] or '').strip()
            keys = (item['meta_keys'] or '').strip()
            desc = (item['meta_desc'] or '').strip()
            data = f'{title} {text} {keys} {desc}'
            lemmas = lemmatize_morphy(data)
            db.save_lemmas(item['id'], lemmas)
    lemmatize(start_id, limit, max_num, total)


def clean_screen_dir(cat_id):
    path = os.path.join(SCREEN_DIR, str(cat_id))
    items = os.listdir(path)
    if len(items) == 0:
        print(f'Empty')
        exit(0)
    items = [int(i.replace('.jpg', '')) for i in items if '_sm' not in i and 'jpg' in i]
    from_db = db.query_all('SELECT id, cat_id FROM urls WHERE id IN %s ORDER BY id', (tuple(items),))
    all = db.query_all('SELECT id FROM urls WHERE cat_id = %s ORDER BY id', (cat_id,))
    all = [i['id'] for i in all]
    db_ids = [i['id'] for i in from_db]
    from_db = {i['id']: i['cat_id'] for i in from_db}
    for_del = [i for i in items if i not in db_ids]
    for_move = [i for i in items if i in db_ids and from_db[i] != cat_id]
    for_add = [i for i in all if i not in items]
    print(f'total screens {len(items)}/ids {len(all)}')
    print('for del ', len(for_del), ',for move ', len(for_move), ',for add ', len(for_add))
    # print(for_add)
    for i in for_del:
        delete_url_screens(i, int(cat_id))
    for i in for_move:
        move_url_screens(i, int(cat_id), from_db[i])


def extract_texts(start_id: int = 0, limit: int = 100, max_num: int = 20000, total: int = 0):
    if total > max_num:
        exit(0)
    print('Start portions since id ', start_id)
    # items = db.get_url_html(start_id, limit)
    items = db.get_url_for_text_update(start_id, limit)
    if len(items) == 0:
        exit(0)
    total += len(items)
    for item in items:
        if item['id'] > start_id:
            start_id = item['id']
        if item['html'].strip() != '':
            title, meta, text = extract_text_title_meta(item['html'])
            db.save_raw_text(item['id'], title, text, meta)
    extract_texts(start_id, limit, max_num, total)


def update_texts(start_id: int = 0, limit: int = 100):
    print('Start portions since id ', start_id)
    items = db.get_texts_for_fix(start_id, limit)
    if len(items) == 0:
        exit(0)
    for item in items:
        if item['id'] > start_id:
            start_id = item['id']

        if item['html_js'].strip() != '':
            title, meta, text = extract_text_title_meta(item['html_js'])
            db.update_raw_text(item['id'], title, text, meta)
    update_texts(start_id, limit)


def guess_lang(start_id: int = 0, limit: int = 100, max_num: int = 20000, total: int = 0):
    if total > max_num:
        exit(0)
    print('Start portions since id ', start_id)
    items = db.get_url_texts_for_lang(start_id, limit)
    # items = db.get_url_texts(start_id, limit)
    if len(items) == 0:
        exit(0)
    total += len(items)
    for item in items:
        if item['id'] > start_id:
            start_id = item['id']
        lang = detect_lang(item['raw_text'])
        db.exec('''UPDATE urls SET lang = %s WHERE id = %s''', (lang, item['id']))
    guess_lang(start_id, limit, max_num, total)


def common_info(data: dict, start_id: int = 0, limit: int = 100):
    print('Start portions since id ', start_id)
    items = db.get_url_markup(start_id, limit)
    if len(items) == 0:
        for key in data.keys():
            if key == 'levels':
                data[key] = dict(sorted(data[key].items()))
            elif key == 'tags':
                data[key] = list(data[key])
                data[key].sort()
        with open(os.path.join(TEMP_DIR, 'common_info.json'), 'w') as f:
            json.dump(data, f, sort_keys=False, ensure_ascii=False)
        return 'Done'
    for item in items:
        if item['id'] > start_id:
            start_id = item['id']
        tags_list = [el['name'] for el in item['markup']]
        data['tags'].update(set(tags_list))
        for tag in tags_list:
            if not re.match(VALID_TAG, tag):
                print('bad tag for ', tag, item['url'])
                data['badtags'].append(item['id'])
                break
        max_level = max([el['level'] for el in item['markup']])
        if max_level > 35:
            data['bigdepth'].append(item['id'])
            print('Big depth', max_level, item['url'])
        if max_level not in data['levels']:
            data['levels'][max_level] = 1
        else:
            data['levels'][max_level] += 1
    common_info(data, start_id, limit)


def import_detected_categories():
    file = os.path.abspath('./data/cats.json')
    with open(file) as fh:
        cats = json.load(fh)
    for cat in cats:
        cat_id = int(cat['cluster']) + 1
        cat_name = ' '.join([w[0] for w in cat['common_words']])[:250]
        db.exec('''INSERT INTO detected_categories (id, name) VALUES (%s, %s)''', (cat_id, cat_name))


if __name__ == '__main__':
    task = sys.argv[1] if len(sys.argv) > 1 else 'info'
    start_id = sys.argv[2] if len(sys.argv) > 2 else 0
    limit = sys.argv[3] if len(sys.argv) > 3 else 50
    if task == 'info':
        data = {'tags': set([]), 'attrs': set([]), 'levels': {}, 'badtags': [], 'bigdepth': []}
        common_info(data, 0, 300)
    elif task == 'cat_import':
        import_detected_categories()
    elif task == 'textify':
        extract_texts(int(start_id), int(limit))
    elif task == 'fixtext':
        update_texts(int(start_id), int(limit))
    elif task == 'lemmatize':
        lemmatize(int(start_id), int(limit))
    elif task == 'remarkup':
        markup_for_train(int(start_id), int(limit))
    elif task == 'guess_lang':
        guess_lang(int(start_id), int(limit))
    elif task == 'clean_screen':
        clean_screen_dir(int(start_id))
    else:
        print('''
        Usage:
           // Aggregate common markup statistic (tags, attributes, levels)
           python -m lib.processing info 
           
           // Extract text, meta-data and titles from html
           python -m lib.processing textify [Optional: start_id: int] [Optional: chunk_size: int]
           
           python -m lib.processing fixtext [Optional: start_id: int] [Optional: chunk_size: int]
           
           // lemmatize texts
           python -m lib.processing lemmatize [Optional: start_id: int] [Optional: chunk_size: int]
           
           // rebuild text markup
           python -m lib.processing remarkup [Optional: start_id: int] [Optional: chunk_size: int]
           
           // Detect language
           python -m lib.processing guess_lang [Optional: start_id: int] [Optional: chunk_size: int]
           
           // Remove screens for directly removed urls
           python -m lib.processing clean_screen [cat_id: int]
        ''')
