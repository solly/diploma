#!/usr/bin/env python
"""
 Load extracted urls from catalog_parser.py with simple requests library for checking actual url states, save html

 :Usage: python -m lib.url_loader [Optional: start_id: int] [Optional: chunk_size: int] [Optional: once : bool]
"""

import logging
import requests
import urllib3
import os
import sys
from .utils.db import Db
from .utils.utils import get_url_host, save_html, LOG_DIR
from .utils.html_process import minify

db = Db()
sys.setrecursionlimit(10**6)
logging.basicConfig(filename=os.path.join(LOG_DIR, 'url_loader.log'), level=logging.INFO)
logging.captureWarnings(True)
urllib3.disable_warnings()


def load_url(url: str):
    hdrs = {
        'content-type': 'text/html',
        #'user-agent': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',
        'referer': 'https://www.google.com/search?newwindow=1&sxsrf=ACYBGNSKYZphRwfwbSIRkX8mWGZxRwJdaA%3A1579450080971&ei=4H4kXrH4OrvAmwXrg6uwCw&q=%D0%BA%D0%B0%D1%82%D0%B0%D0%BB%D0%BE%D0%B3+%D1%81%D0%B0%D0%B9%D1%82%D0%BE%D0%B2&oq=%D0%BA%D0%B0%D1%82%D0%B0%D0%BB%D0%BE%D0%B3+%D1%81%D0%B0%D0%B9%D1%82%D0%BE%D0%B2&gs_l=psy-ab.3..0l10.217171.221389..221675...3.4..0.303.2371.1j17j0j1......0....1..gws-wiz.....0..0i71j0i67j0i131j0i10i1i67j0i10i1i42j0i10i1j0i67i70i249.sJ27JbqchEo&ved=0ahUKEwix6JCIhpDnAhU74KYKHevBCrYQ4dUDCAs&uact=5'
    }
    try:
        response = requests.get(url, headers=hdrs, verify=False, allow_redirects=True, timeout=45)
        real_url = response.url.strip('/')
        content = response.text
        encoding = response.encoding.lower()
        if encoding == 'cp1251':
            encoding = 'windows-1251'
        if encoding == 'utf-8':
            encoding = 'utf8'
        return True, content, real_url, encoding
    except Exception as e:
        logging.error('Fail url loading ' + url, exc_info=False)
        return False, '', url, ''


def load(start_id: int = 0, limit: int = 50, once: bool = False):
    print('Start portion since id =', start_id)
    items = db.get_catalog_not_loaded_urls(start_id, limit)
    if len(items) > 0:
        for item in items:
            if item['id'] > start_id:
                start_id = item['id']
            success, html, url, enc = load_url(item['url'])
            html.replace('\x00', '')
            if success:
                host = get_url_host(url)
                html_min = minify(html)
                url_id = None
                try:
                   url_id = db.save_url_content(item['cat_id'], url, host, html_min, enc)
                except Exception as e:
                    logging.exception('Url save exception')
                    print('Fail save url', item['url'], host, enc)
                    db.delete_catalog_url(item['id'])
                if url_id:
                    save_html(url_id, item['cat_id'], html_min)
                    db.mark_catalog_url_loaded(item['id'])
                else:
                    print('Fail save ', url)
                    db.delete_catalog_url(item['id'])
            else:
                print('Fail load ', url)
                db.delete_catalog_url(item['id'])
        if not once:
            load(start_id, limit, once)


if __name__ == '__main__':
    start_id = sys.argv[1] if len(sys.argv) > 1 else 0
    limit = sys.argv[2] if len(sys.argv) > 2 else 50
    once = True if len(sys.argv) > 3 else False
    load(int(start_id), int(limit), once)
