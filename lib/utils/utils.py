from hashlib import md5
from urllib.parse import urlparse
import os
import json
import shutil

LOG_DIR = os.path.abspath('./logs/')
TEMP_DIR = os.path.abspath('./tmp/')
SCREEN_DIR = os.path.abspath('./app/static/htmldb/')


def get_hash(s: str) -> str:
    m = md5()
    m.update(s.encode())
    return m.hexdigest()


def get_url_host(url: str) -> str:
    parsed = urlparse(url)
    return parsed.netloc.lower().replace('www.', '')


def save_temp(filename: str, content: str) -> None:
    file_path = os.path.join(TEMP_DIR, filename)
    with open(file_path, 'w') as fh:
        fh.write(content)


def save_html(idx: int, cat_id: int, data: str, name_tpl: str = '%s.html') -> None:
    data_dir = os.path.join(SCREEN_DIR, str(cat_id))
    name = name_tpl % str(idx)
    if not os.path.exists(data_dir):
        os.mkdir(data_dir)
    with open(os.path.join(data_dir, name), 'w') as f:
        f.write(data)


def save_json(idx: int, cat_id: int, data: dict, name_tpl: str = '%s.json') -> None:
    data_dir = os.path.join(SCREEN_DIR, str(cat_id))
    name = name_tpl % str(idx)
    if not os.path.exists(data_dir):
        os.mkdir(data_dir)
    with open(os.path.join(data_dir, name), 'w') as f:
        json.dump(data, f, ensure_ascii=False, sort_keys=False)


def screen_path_for_new(idx: int, name_tpl: str = '%s.jpg') -> str:
    data_dir = os.path.join(SCREEN_DIR, 'new')
    name = name_tpl % str(idx)
    if not os.path.exists(data_dir):
        os.mkdir(data_dir)
    return os.path.join(data_dir, name)


def screen_path(idx: int, cat_id: int, name_tpl: str = '%s.jpg') -> str:
    data_dir = os.path.join(SCREEN_DIR, str(cat_id))
    name = name_tpl % str(idx)
    if not os.path.exists(data_dir):
        os.mkdir(data_dir)
    return os.path.join(data_dir, name)


def move_url_screens(idx: int, old_cat: int, new_cat: int):
    old_path = screen_path(idx, old_cat)
    old_sm_path = screen_path(idx, old_cat, '%s_sm.jpg')
    if os.path.exists(old_path):
        shutil.move(old_path, screen_path(idx, new_cat))
    if os.path.exists(old_sm_path):
        shutil.move(old_sm_path, screen_path(idx, new_cat, '%s_sm.jpg'))


def delete_url_screens(idx: int, cat_id: int):
    old_path = screen_path(idx, cat_id)
    old_sm_path = screen_path(idx, cat_id, '%s_sm.jpg')
    if os.path.exists(old_path):
        os.remove(old_path)
    if os.path.exists(old_sm_path):
        os.remove(old_sm_path)
