import nltk

nltk.download("stopwords")
import re
from pymorphy2 import MorphAnalyzer
from collections import Counter
from nltk.corpus import stopwords
from string import punctuation
from guess_language import guess_language

PUNCTUATION = punctuation + '\u2014\u2013\u2012\u2010\u2212' + '«»‹›‘’“”„`<>©«»№»«'
WORD_REGEXP = re.compile(r"([^\w_\u2019\u2010\u002F-]|[+])")
STOPWORDS = stopwords.words('russian')
CYRILLIC = re.compile(r'[А-Яа-яЁё]')
DIGIT = re.compile(r'\d')
NOT_PHONE_OR_DATE = re.compile(r'[^0-9\(\)\-\s\+\.\\\_]')
BAD_UNICODE = re.compile(r'u\d{4}')
BAD_CHARS = re.compile(r'<>=@')
LEMMA_STOP_WORDS = [
    'наш', 'ваш', 'дробный', 'год', 'the', 'value', 'default_value', 'свой', 'весь', 'каждый', 'другой', 'который',
    'мочь', 'name', 'array', 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'новый', 'самый', 'самая', 'самое',
    'июль', 'август', 'http', 'www', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь', 'rss', 'cookies', 'en', 'ru', 'мимо',
    'мими', 'рубль', 'сайт', 'компания', 'очень', 'такой', 'логин', 'пароль', 'регистрация', 'главный']


def detect_lang(text: str) -> str:
    lang = guess_language(text)
    if lang != 'ru':
        rus_words = re.findall(r'[А-Яа-яЁё]{4,}', text)
        if len(rus_words) > 5 and not re.match(r'[їє]', text):
            lang = 'ru'
    return lang.lower()


def text_cleaner(text):
    for word in text.split(' '):
        word = word.lower()
        if not BAD_CHARS.search(word) \
                and not word.startswith('\\') \
                and not word.startswith('http') \
                and not word.startswith('www'):
            yield word


def word_splitter(text):
    for word in WORD_REGEXP.split(' '.join([w for w in text_cleaner(text)])):
        if word and not word.isspace() \
                and not word.isdigit() \
                and len(word) > 2 \
                and word not in PUNCTUATION \
                and word not in STOPWORDS \
                and word not in LEMMA_STOP_WORDS \
                and not BAD_UNICODE.search(word) \
                and NOT_PHONE_OR_DATE.search(word):
            if CYRILLIC.search(word) and DIGIT.search(word):
                word = re.sub(r'[\d\-]+', '', word)
            yield word


def lemmatize_morphy(text: str) -> list:
    morph = MorphAnalyzer()
    lemmas = []
    for word in word_splitter(text):
        p = morph.parse(word)
        tags = [v.tag.POS for v in p]
        if len(tags) and not len(set(tags).intersection(['NPRO', 'PRED', 'PREP', 'CONJ', 'PRCL', 'INTJ'])):
            lemma = p[0].normal_form
            if lemma not in LEMMA_STOP_WORDS:
                lemmas.append(lemma)
    return lemmas


def lemmatize_mystem(text: str) -> list:
    m = Mystem()
    text = ' '.join([w for w in word_splitter(text)])
    lemmas = m.lemmatize(text)
    lemmas = [i for i in lemmas if i != ' ']
    print(' '.join(lemmas))
    common = Counter(lemmas).most_common(20)
    print(common)
    return lemmas
