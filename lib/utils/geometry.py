from math import sqrt


class Point:
    x = None
    y = None

    def __init__(self, x, y):
        self.x, self.y = x, y

    def __str__(self):
        return "%6.1f, %6.1f" % (self.x, self.y)

    def __eq__(self, obj):
        return obj.x == self.x and obj.y == self.y

    def distance_to_point(self, p) -> float:
        return sqrt((self.x - p.x) ** 2 + (self.y - p.y) ** 2)


class Rect:
    viewport_width = 1024
    screen_height = 748
    l_top = None
    r_top = None
    l_bot = None
    r_bot = None
    center = None
    width = None
    height = None
    vl_top = None
    vr_bot = None

    def __init__(self, x, y, x1, y1):
        self.l_top = Point(x, y)
        self.r_bot = Point(x1, y1)
        self.r_top = Point(x + x1, y)
        self.l_bot = Point(x, y + y1)
        self.width = abs(abs(x1) - abs(x))
        self.height = abs(abs(y1) - abs(y))
        self.center = Point(x + self.width / float(2), y + self.height / float(2))
        self.vl_top = Point(min(max(self.l_top.x, 0), self.viewport_width), max(self.l_top.y, 0))
        self.vr_bot = Point(min(max(self.r_bot.x, 0), self.viewport_width), max(self.r_bot.y, 0))

    def __str__(self):
        return f'({self.l_top}, {self.l_bot})({self.r_top, self.r_bot})'

    def square(self) -> int:
        return round(self.width * self.height)

    def visible_square(self) -> int:
        return round(abs((self.vr_bot.x - self.vl_top.x) * (self.vr_bot.y - self.vl_top.y)))

    def is_visible(self) -> bool:
        return self.visible_square() > 0

    def is_full_visible(self) -> bool:
        return self.visible_square() == self.square()

    def is_inside(self, r) -> bool:
        return self.vl_top.x <= r.vl_top.x \
               and self.vr_bot.x <= r.vr_bot.x \
               and self.vl_top.y <= r.vl_top.y \
               and self.vr_bot.y <= r.vr_bot.y

    def is_include(self, r) -> bool:
        return r.is_inside(self)

    def is_same_row(self, r) -> bool:
        return self.vl_top.y == r.vl_top.y and self.vr_bot.y == r.vr_bot.y

    def is_same_column(self, r) -> bool:
        return self.vl_top.x == r.vl_top.x and self.vr_bot.x == r.vr_bot.x

    def is_on_first_screen(self, even_partially: bool = True) -> bool:
        return self.viewport_width > self.vl_top.x > 0 and \
               self.screen_height > self.vl_top.y > 0 and \
               (not even_partially or (self.screen_height > self.vr_bot.y and self.viewport_width > self.vr_bot.x))