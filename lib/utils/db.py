import psycopg2
import json
from psycopg2.extras import DictCursor, Json
from psycopg2.extensions import register_adapter
import config
import os


class Db:
    """
      Parser database class
    """

    def __init__(self):
        kwargs = {
            'dbname': os.getenv('DB_NAME'),
            'user': os.getenv('DB_USER'),
            'password': os.getenv('DB_PASS'),
            'host': os.getenv('DB_HOST'),
            'port': os.getenv('DB_PORT'),
        }
        self.db = psycopg2.connect(**kwargs)
        register_adapter(dict, Json)

    def exec(self, query: str, params: tuple = ()):
        with self.db.cursor() as cursor:
            cursor.execute(query, params)
            self.db.commit()

    def query_one(self, query: str, params: tuple = ()):
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(query, params)
            return cursor.fetchone()

    def query_all(self, query: str, params: tuple = ()):
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(query, params)
            return cursor.fetchall()

    def save_catalog(self, data: dict):
        with self.db.cursor() as cursor:
            cursor.execute('SELECT COUNT(id) FROM categories')
            exists = cursor.fetchone()[0]
            if exists == 0:
                for name in data.keys():
                    cursor.execute('INSERT INTO categories (name) VALUES (%s)', (name,))
                self.db.commit()
                print('Catalog successfully saved')
            else:
                print('Catalog already saved')

    def find_category_id(self, name: str) -> int:
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute('SELECT id FROM categories WHERE name=%s', (name,))
            cat = cursor.fetchone()
            return cat['id']

    def get_categories(self, with_count: bool = True):
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            if with_count:
                sql = '''
                SELECT u.cat_id as id, c.name, count(u.id) FROM urls u 
                INNER JOIN categories c ON u.cat_id= c.id
                GROUP BY u.cat_id, c.name ORDER BY cat_id
                '''
            else:
                sql = '''SELECT id, name FROM categories'''
            cursor.execute(sql)
            return cursor.fetchall()

    def save_catalog_url(self, url: str, title: str, cat_id: int):
        with self.db.cursor() as cursor:
            cursor.execute(
                '''
                INSERT INTO catalog_urls (cat_id, url, url_title)
                VALUES (%s, %s, %s)
                ON CONFLICT (url) DO NOTHING 
                ''', (cat_id, url, title)
            )
            self.db.commit()

    def save_new_url(self, url: str, cat_id=None):
        with self.db.cursor() as cursor:
            cursor.execute(
                '''
                INSERT INTO urls (cat_id, category_id, url)
                VALUES (%s, %s, %s)
                ON CONFLICT (url) DO NOTHING 
                RETURNING id
                ''', (cat_id, None, url)
            )
            self.db.commit()
            r = cursor.fetchone()
            _id = r[0] if r else False
            if _id:
                cursor.execute(
                    '''
                    INSERT INTO url_html (url_id) 
                    VALUES (%s)
                    ON CONFLICT (url_id) DO NOTHING 
                    ''', (_id,)
                )
                cursor.execute(
                    '''
                    INSERT INTO url_markup (url_id) 
                    VALUES (%s)
                    ON CONFLICT (url_id)  DO NOTHING 
                    ''', (_id,)
                )
                cursor.execute(
                    '''
                    INSERT INTO url_texts (url_id) 
                    VALUES (%s)
                    ON CONFLICT (url_id)  DO NOTHING 
                    ''', (_id,)
                )
            self.db.commit()
            return _id

    def get_catalog_not_loaded_urls(self, start_id: int = 0, limit: int = 50):
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(
                ''' SELECT id, cat_id, url FROM catalog_urls WHERE id > %s AND loaded=%s ORDER BY id LIMIT %s ''',
                [start_id, False, limit]
            )
            return cursor.fetchall()

    def mark_catalog_url_loaded(self, _id: int):
        with self.db.cursor() as cursor:
            cursor.execute(
                ''' UPDATE catalog_urls SET loaded=True WHERE id = %s''', [_id]
            )
            self.db.commit()

    def delete_catalog_url(self, _id: int):
        with self.db.cursor() as cursor:
            cursor.execute(
                ''' DELETE FROM catalog_urls WHERE id = %s''', [_id]
            )
            self.db.commit()

    def mark_load_error(self, _id: int):
        with self.db.cursor() as cursor:
            cursor.execute(
                ''' UPDATE urls SET load_err=True WHERE id = %s''', [_id]
            )
            self.db.commit()

    def get_content_not_loaded_urls(self, start_id: int = 0, limit: int = 50):
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            sql = ''' SELECT id, cat_id, url FROM urls 
            WHERE id > %s AND has_markup=False AND load_err=False ORDER BY id  LIMIT %s '''
            cursor.execute(sql, [start_id, limit])
            return cursor.fetchall()

    def get_content_err_loaded_urls(self, start_id: int = 0, limit: int = 50):
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            sql = ''' SELECT id, cat_id, url FROM urls 
            WHERE id > %s AND has_markup=False AND load_err=True ORDER BY id  LIMIT %s '''
            cursor.execute(sql, [start_id, limit])
            return cursor.fetchall()

    def get_content_bad_loaded_urls(self, start_id: int = 0, limit: int = 50):
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            sql = ''' SELECT urls.id, cat_id, url FROM urls 
            INNER JOIN url_html uh on urls.id = uh.url_id
            INNER JOIN url_markup um on urls.id = um.url_id
            WHERE urls.id >= %s AND (uh.html_js='' OR uh.html_js IS NULL OR um.markup IS NULL) AND urls.load_err = False
            ORDER BY urls.id  LIMIT %s '''
            cursor.execute(sql, [start_id, limit])
            return cursor.fetchall()

    def get_url_markup(self, start_id: int = 0, limit: int = 50):
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            sql = '''
            SELECT u.id, u.url, u.cat_id, m.markup 
            FROM url_markup m INNER JOIN urls u on m.url_id = u.id
            WHERE m.markup IS NOT NULL AND m.url_id > %s ORDER BY m.url_id  LIMIT %s
            '''
            cursor.execute(sql, [start_id, limit])
            return cursor.fetchall()

    def get_url_html(self, start_id: int = 0, limit: int = 50):
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            sql = '''
            SELECT u.id, u.url, u.cat_id, h.html 
            FROM url_html h INNER JOIN urls u on h.url_id = u.id
            WHERE h.html IS NOT NULL AND h.url_id > %s ORDER BY h.url_id  LIMIT %s
            '''
            cursor.execute(sql, [start_id, limit])
            return cursor.fetchall()

    def get_texts_for_fix(self, start_id: int = 0, limit: int = 50):
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            sql = '''
              SELECT url_id as id, html_js FROM url_html
              WHERE url_id in 
              (SELECT url_id FROM url_texts WHERE  length(raw_text) < 500)
              AND url_id > %s ORDER BY url_id LIMIT %s
            '''
            cursor.execute(sql, [start_id, limit])
            return cursor.fetchall()

    def get_url_for_text_update(self, start_id: int = 0, limit: int = 50):
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            sql = '''
            SELECT u.id, u.url, u.cat_id, h.html_js
            FROM url_html h INNER JOIN urls u on h.url_id = u.id
            WHERE u.id IN
            (
                SELECT u.id FROM url_texts LEFT JOIN urls u on url_texts.url_id = u.id
                WHERE raw_text is null or raw_text = ''
                or raw_text ilike %s or raw_text ilike %s
                or raw_text ilike %s or raw_text ilike %s
            ) AND u.id > %s ORDER BY u.id  LIMIT %s
            '''
            cursor.execute(sql, ['%browsehappy%', '%browser%', "%браузер%", '%cookies%', start_id, limit])
            return cursor.fetchall()

    def get_url_texts(self, start_id: int = 0, limit: int = 50):
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            sql = '''
            SELECT u.id, u.url, u.cat_id, h.raw_text, h.title, h.meta_desc, h.meta_keys 
            FROM url_texts h INNER JOIN urls u on h.url_id = u.id
            WHERE h.raw_text IS NOT NULL AND h.lemmas is null 
            AND h.url_id > %s ORDER BY h.url_id  LIMIT %s
            '''
            cursor.execute(sql, [start_id, limit])
            return cursor.fetchall()

    def get_lemmas(self, start_id: int = 0, limit: int = 50):
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            sql = '''
            SELECT u.id, u.url, c.name, h.lemmas 
            FROM url_texts h INNER JOIN urls u on h.url_id = u.id
            INNER JOIN categories c on u.cat_id = c.id
            WHERE  h.url_id > %s AND u.category_id is null AND h.lemmas is not null ORDER BY h.url_id LIMIT %s
            '''
            cursor.execute(sql, [start_id, limit])
            return cursor.fetchall()

    def get_url_texts_for_lang(self, start_id: int = 0, limit: int = 50):
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            sql = '''
            SELECT u.id, u.url, u.cat_id, h.raw_text, h.title, h.meta_desc, h.meta_keys 
            FROM url_texts h INNER JOIN urls u on h.url_id = u.id
            WHERE h.raw_text IS NOT NULL AND h.raw_text != '' 
            AND h.url_id > %s AND u.lang !='ru'
            ORDER BY h.url_id  LIMIT %s
            '''
            cursor.execute(sql, [start_id, limit])
            return cursor.fetchall()

    def get_url_htmljs(self, start_id: int = 0, limit: int = 50):
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            sql = '''
            SELECT u.id, u.url, u.cat_id, h.html_js 
            FROM url_html h INNER JOIN urls u on h.url_id = u.id
            WHERE h.html_js IS NOT NULL AND h.url_id > %s ORDER BY h.url_id  LIMIT %s
            '''
            cursor.execute(sql, [start_id, limit])
            return cursor.fetchall()

    def save_detected_cat(self, url_id: int, cat_id: int):
        with self.db.cursor() as cursor:
            cursor.execute(''' UPDATE urls SET category_id = %s WHERE id = %s''', (cat_id, url_id))
            self.db.commit()

    def save_url_content(self, cat_id: int, url: str, host: str, html: str, encoding: str, is_main: bool = True):
        with self.db.cursor() as cursor:
            cursor.execute(
                ''' INSERT INTO urls (cat_id, url, host, charset, is_main)
                 VALUES (%s, %s, %s, %s, %s)
                 ON CONFLICT (url) DO NOTHING
                 RETURNING id
                 ''', (cat_id, url, host, encoding, is_main)
            )
            self.db.commit()
            r = cursor.fetchone()
            id = r[0] if r else False
            if id:
                cursor.execute(
                    '''
                    INSERT INTO url_html (url_id, html) 
                    VALUES (%s, %s)
                    ON CONFLICT (url_id) 
                    DO UPDATE SET html = %s
                    ''', (id, html, html)
                )
            self.db.commit()
            return id

    def save_browser_content(self, url_id: int, html_js: str, markup: dict, status: int):
        len_markup = len(markup)
        markup_json = json.dumps(markup, sort_keys=False, ensure_ascii=False)
        with self.db.cursor() as cursor:
            try:
                cursor.execute(''' UPDATE urls SET status_code = %s, has_markup=True, load_err = False 
                WHERE id = %s ''',
                               (status, url_id)
                               )
                cursor.execute(''' UPDATE url_html SET html_js = %s WHERE url_id = %s ''', (html_js, url_id))

                cursor.execute(
                    '''
                    INSERT INTO url_markup (url_id, markup, elem_count) 
                    VALUES (%s, %s, %s)
                    ON CONFLICT (url_id) 
                    DO UPDATE SET markup = %s, elem_count = %s
                    ''', (url_id, markup_json, len_markup, markup_json, len_markup)
                )
            finally:
                self.db.commit()

    def update_raw_text(self, url_id: int, title, text: str, meta: dict):
        with self.db.cursor() as cursor:
            sql = '''UPDATE url_texts SET title = %s, meta_keys = %s, meta_desc = %s, raw_text = %s WHERE url_id = %s'''
            try:
                cursor.execute(sql, (title, meta['keys'], meta['desc'], text, url_id))
            finally:
                self.db.commit()

    def save_raw_text(self, url_id: int, title, text: str, meta: dict):
        with self.db.cursor() as cursor:
            sql = '''
             INSERT INTO url_texts (url_id, title, meta_keys, meta_desc, raw_text) VALUES (%s, %s, %s, %s, %s)
             ON CONFLICT (url_id)
             DO UPDATE SET title = %s, meta_keys = %s, meta_desc = %s, raw_text = %s
             '''
            try:
                cursor.execute(
                    sql, (url_id, title, meta['keys'], meta['desc'], text, title, meta['keys'], meta['desc'], text)
                )
            finally:
                self.db.commit()

    def save_lemmas(self, url_id: int, lemmas: list):
        with self.db.cursor() as cursor:
            sql = '''UPDATE url_texts SET lemmas = %s, lemmas_count = %s WHERE url_id = %s '''
            try:
                cursor.execute(sql, (json.dumps(lemmas, ensure_ascii=False), len(lemmas), url_id))
            finally:
                self.db.commit()

    def save_markup(self, url_id: int, markup: list):
        with self.db.cursor() as cursor:
            sql = '''UPDATE url_markup SET markup = %s WHERE url_id = %s '''
            try:
                cursor.execute(sql, (json.dumps(markup, ensure_ascii=False), url_id))
            finally:
                self.db.commit()

    def save_train_markup(self, url_id: int, markup: list, depth: int, height: int):
        with self.db.cursor() as cursor:
            sql = '''UPDATE url_markup SET train_markup = %s, max_depth = %s, height= %s
             WHERE url_id = %s '''
            try:
                cursor.execute(sql, (json.dumps(markup, ensure_ascii=False), depth, height, url_id))
            finally:
                self.db.commit()

    def normalize_encoding_values(self):
        with self.db.cursor() as cursor:
            try:
                cursor.execute(''' UPDATE urls SET charset='utf8' where charset in ('utf-8', 'UTF-8') ''')
                cursor.execute(
                    ''' UPDATE urls SET charset='windows-1251' where charset in ('cp1251', 'WINDOWS-1251') '''
                )
                cursor.execute(''' UPDATE urls SET charset='iso-8859-1' where charset in ('ISO-8859-1') ''')
            finally:
                self.db.commit()
