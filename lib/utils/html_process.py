import htmlmin
import re
from bs4 import BeautifulSoup, NavigableString, Comment, CData


def parent_current_path(xpath: str) -> tuple:
    xpath_parts = xpath.split('/')
    parent = '/'.join(xpath_parts[:-1])
    current = xpath_parts[-1]
    del xpath_parts
    return parent, current


def markup_max_depth(markup) -> int:
    return max([el['l'] for el in markup])


def markup_max_element_height(markup) -> int:
    return max([round(abs(max(el['y1'], 0) - max(el['y'], 0))) for el in markup])


def markup_max_visual_height(markup) -> int:
    return max([el['y1'] for el in markup])


def train_markup(markup) -> list:
    nmarkup = []
    xpath_map = {el['id']: el['cnt'] for el in markup}
    for el in markup:
        parent, current = parent_current_path(el['id'])
        if parent != '/html' and parent not in xpath_map:
            print(f'  !!!   {parent} for {el["id"]} is absent')
            continue
        if current not in ['html', 'body']:
            parent_size = xpath_map[parent]
            if parent_size < 1:
                print('  !!!   Inconsistent tree data for ', parent)
            nchild = round(1 / parent_size, 8)
        elif current == 'body':
            nchild = 1
        else:
            nchild = 0

        data = [
            el['id'],
            el['tag'],
            el['l'],
            nchild,
            el['cnt'],
            round(min(max(el['x'], 0), 1024)),
            round(min(max(el['x1'], 0), 1024)),
            round(max(el['y'], 0)),
            round(max(el['y1'], 0)),
            len(el['v']),
            0
        ]
        nmarkup.append(data)
    return nmarkup


def minify(html: str) -> str:
    try:
        min = htmlmin.minify(html, True, True)
        return min.replace('\x00', '')
    except Exception as e:
        return html.replace('\x00', '')


def extract_text_title_meta(html: str) -> tuple:
    q = BeautifulSoup(html, 'html.parser')
    title = q.title.string if q.title else ''
    meta = {'keys': '', 'desc': ''}
    desc = q.find(lambda tag: tag.name == 'meta' and tag.has_attr('name') and tag['name'] == 'description')
    if desc and desc.has_attr('content'):
        meta['desc'] = desc['content'].strip()[:2000]
    keys = q.find(lambda tag: tag.name == 'meta' and tag.has_attr('name') and tag['name'] == 'keywords')
    if keys and keys.has_attr('content'):
        meta['keys'] = keys['content'].strip()[:2000]
    texts = []
    comments = q.findAll(text=lambda text: isinstance(text, CData) or
                                           (isinstance(text, Comment) and text.find('if') != -1))
    [comment.extract() for comment in comments]
    body = q.body
    if not body:
        return '', meta, ''
    for tag in ['script', 'noscript', 'noindex', 'style', 'code', 'iframe', 'notypograf']:
        items = body.find_all(tag)
        if len(items):
            for item in items:
                item.extract()

    for node in body.children:
        if isinstance(node, NavigableString):
            valid = True
            for el in ['script', 'noscript', 'noindex', 'style', 'text/css', 'iframe', 'notypograf', 'var', 'function']:
                if el in str(node):
                    valid = False
            if valid:
                texts.append(str(node))
        else:
            texts.append(' '.join(node.stripped_strings))
    return title, meta, ' '.join(texts).strip()
