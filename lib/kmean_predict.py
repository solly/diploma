"""
 Predict categories by lemmas based on clusterization
  @see https://colab.research.google.com/drive/1OoHGXJKKczc_QAyr0A-v0fM4MgrSKkcZ?authuser=1#scrollTo=9SnKAPdwHZ6w
 :Usage: python -m lib.kmean_predict
"""
import pandas as pd
import numpy as np
import os
import sys

from sklearn.preprocessing import StandardScaler, LabelEncoder, OneHotEncoder
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import normalize
import joblib

from collections import Counter
from .utils.db import Db

sys.setrecursionlimit(10**6)

db = Db()
n_components = 118
model_path = os.path.abspath('./data/kmean.sav')
vectorizor_path = os.path.abspath('./data/vec.sav')
pca_path = os.path.abspath('./data/pca.sav')
cats_path = os.path.abspath('./data/cats.json')
kmean = joblib.load(model_path)
vectorizer = joblib.load(vectorizor_path)
pca = joblib.load(pca_path)


def predict_item(lemmas):
    texts = np.array([' '.join(lemmas)])
    tf_idf = vectorizer.transform(texts)
    tf_idf_array = normalize(tf_idf).toarray()
    pca_tf_idf = pca.transform(tf_idf_array)
    del tf_idf, tf_idf_array
    result = kmean.predict(pca_tf_idf)
    return int(result[0])+1


def predict_clusters(start_id: int = 0, limit: int = 10):
    print('Start portions since id ', start_id)
    items = db.get_lemmas(start_id, limit)
    if len(items) == 0:
        exit(0)
    for i, it in enumerate(items):
        if it['id'] > start_id:
            start_id = it['id']
        try:
            cat_id = predict_item(it['lemmas'])
        except Exception as e:
            print(it['lemmas'])
            raise e
        print(it['id'], it['name'], it['url'], cat_id)
        db.save_detected_cat(url_id=it['id'], cat_id=cat_id)
    predict_clusters(start_id, limit)


if __name__ == '__main__':
    start_id = sys.argv[1] if len(sys.argv) > 1 else 0
    limit = sys.argv[2] if len(sys.argv) > 2 else 2
    predict_clusters(int(start_id), int(limit))