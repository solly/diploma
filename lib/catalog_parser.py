#!/usr/bin/env python
"""
 Parse https://cmsmagazine.ru/sites/, extract catalog categories and site urls per category

 :Usage: python -m lib.catalog_parser
"""
import requests
from pyquery.pyquery import PyQuery as pq
import json
import os
from .utils.db import Db
from .utils.utils import get_hash, TEMP_DIR

db = Db()


def get_html(url: str, with_cache: bool = False) -> str:
    url_hash = get_hash(url)
    cache_path = os.path.join(TEMP_DIR, url_hash)
    if with_cache and os.path.exists(cache_path):
        with open(cache_path) as f:
            s = f.read()
        return s
    hdrs = {
        'content-type': 'text/html',
        'user-agent': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
        'referer': 'https://www.google.com/search?newwindow=1&sxsrf=ACYBGNSKYZphRwfwbSIRkX8mWGZxRwJdaA%3A1579450080971&ei=4H4kXrH4OrvAmwXrg6uwCw&q=%D0%BA%D0%B0%D1%82%D0%B0%D0%BB%D0%BE%D0%B3+%D1%81%D0%B0%D0%B9%D1%82%D0%BE%D0%B2&oq=%D0%BA%D0%B0%D1%82%D0%B0%D0%BB%D0%BE%D0%B3+%D1%81%D0%B0%D0%B9%D1%82%D0%BE%D0%B2&gs_l=psy-ab.3..0l10.217171.221389..221675...3.4..0.303.2371.1j17j0j1......0....1..gws-wiz.....0..0i71j0i67j0i131j0i10i1i67j0i10i1i42j0i10i1j0i67i70i249.sJ27JbqchEo&ved=0ahUKEwix6JCIhpDnAhU74KYKHevBCrYQ4dUDCAs&uact=5'
    }
    response = requests.get(url, headers=hdrs)
    if with_cache:
        with open(cache_path, 'w') as f:
            f.write(response.text)
    return response.text


def load_catalog(url='https://cmsmagazine.ru/sites/', base_url='https://cmsmagazine.ru/sites/') -> dict:
    html = get_html(url, True)
    dict = {}
    q = pq(html, url=base_url)
    q.make_links_absolute()
    for node in q.find('div.countriesList a').items():
        dict[node.text()] = node.attr('href')
    print(dict)
    return dict


def load_category(cat_id: int, url: str):
    url += '&pn=all'
    print(url)
    html = get_html(url, True)
    q = pq(html)
    items = q.find('div.expIconYesNo a.js-link').items()
    items_count = len(list(items))
    print('Founded items ', items_count)
    saved = 0
    for node in q.find('div.expIconYesNo a.js-link').items():
        href = node.attr('_href')
        title = node.text()
        if saved == 0:
            print(href, title)
        db.save_catalog_url(href, title, cat_id)
        saved += 1
    print('Saved', saved)


if __name__ == '__main__':
    catalog_path = os.path.join(TEMP_DIR, 'catalogcm.json')
    if os.path.exists(catalog_path):
        with open(catalog_path) as f:
            catalog = json.load(f)
    else:
        catalog = load_catalog()
        with open(catalog_path, 'w') as f:
            json.dump(catalog, f, sort_keys=False, ensure_ascii=False, indent=4)
    db.save_catalog(catalog)
    skip = ['Авто/Мото']
    for name, url in catalog.items():
        if name not in skip:
            cat_id = db.find_category_id(name)
            print('load category:', name)
            load_category(cat_id, url)
