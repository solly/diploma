"""
Prototype for whole new url processing
"""

import os
import sys
import json
import asyncio
from .utils.db import Db
from lib.url_loader import load_url as load_html
from .utils.utils import get_url_host, save_html, LOG_DIR, screen_path_for_new, screen_path
from .utils.html_process import minify, extract_text_title_meta, train_markup
from lib.browsery import run_browser, load_url
from .utils.text_process import lemmatize_morphy, detect_lang
from lib.kmean_predict import predict_item

db = Db()


async def add_url(url: str, cat_id:int = 7):
    url_id = db.save_new_url(url, cat_id)

    # @TODO: separate and make queueable
    try:
        print('try to load url')
        success, html, url, enc = load_html(url)
        html.replace('\x00', '')
        host = get_url_host(url)
        html_min = minify(html)
        print('effective_url', url)
        db.exec('''UPDATE urls SET host = %s, url = %s WHERE id = %s''', (str(host), str(url), int(url_id)))
        db.exec('''UPDATE url_html SET html = %s WHERE id = %s''', (html_min, int(url_id)))
    except Exception as e:
        db.exec('''DELETE FROM urls where id = %s''', params=(int(url_id),))
        print(e)
        raise Exception('Fail load url')

    print('try to load url with browser')
    browser = await run_browser()
    try:
        html, nodes, status = await load_url(browser, url, screen_path(url_id, cat_id))
        if html and nodes:
            html_min = minify(html.replace('\x00', '').replace('\u0000', ''))
            markup = train_markup(nodes)
            db.exec('''UPDATE url_html SET html_js = %s WHERE url_id = %s''', (html_min, int(url_id)))
            db.exec('''UPDATE urls SET status_code = %s WHERE id = %s''', (status, int(url_id)))
            db.exec(
                '''UPDATE url_markup SET markup = %s, train_markup = %s, elem_count = '%s' WHERE url_id = %s''',
                (json.dumps(nodes, ensure_ascii=False), json.dumps(markup), len(markup), int(url_id)))
    except Exception as e:
        db.exec('''DELETE FROM urls where id = %s''', (int(url_id),))
        print(e)
        raise Exception('Fail load url by browser')
    finally:
        if browser is not None:
            browser.close()

    try:
        print('extract url data')
        title, meta, text = extract_text_title_meta(html)
        print('lemmatize')
        lemmas = lemmatize_morphy(text)
        db.exec(''' 
        UPDATE url_texts SET title = %s, meta_keys = %s, meta_desc = %s, raw_text = %s, lemmas = %s, lemmas_count = %s
        WHERE url_id = %s
        ''', (title, meta['keys'], meta['desc'], text, json.dumps(lemmas, ensure_ascii=False), len(lemmas), url_id)
        )
        print('detect lang')
        lang = detect_lang(text)
        category_id = predict_item(lemmas)
        # move url screen from new to cat_id
        db.exec('''UPDATE urls SET lang = %s, category_id = %s WHERE id = %s''', (lang, category_id, url_id))
    except Exception as e:
        print(e)
        exit(1)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage:', 'python -m lib.add_url http://example.com')
        exit(0)
    else:
        url = sys.argv[1]
        # @TODO: validate
        asyncio.get_event_loop().run_until_complete(add_url(url))




